const ethers = require('ethers')
// const identity = require('@caelum-tech/identity/build/contracts/Identity.json')
const identity = require('@caelum-tech/identity-manager-contracts/build/contracts/Identity.json')
// These are magic addresses to find contracts created in the genesis block
const systemAddresses = {
  identityContract: '0x0000000000000000000000000000000000000010'
}

function identityConstructor (ownerAccount) {
  const idIntf = new ethers.utils.Interface(identity.abi)
  return idIntf.deployFunction.encode(identity.bytecode, [ownerAccount])
}

/**
 * Build a fast-sealing genesis block spec
 * @param {string} name
 * @param {string} networkID
 * @param {string} ownerAddress (of identity contract)
 * @param {stringArray} validators
 * @returns {string} genesis block spec
 */
function buildFastSpec (name, networkID, ownerAddress, validators) {
  const engine = {
    'instantSeal': {
      'params': {
      }
    }
  }
  const genesis = {
    'seal': {
      'generic': '0x0'
    },
    'difficulty': '0x20000',
    'gasLimit': '0x989680'
  }
  return build(engine, genesis, name, networkID, ownerAddress, validators)
}

/**
 * Build a production-grade genesis block spec
 * @param {string} name
 * @param {string} networkID
 * @param {string} ownerAddress (of identity contract)
 * @param {stringArray} validators
 * @returns {string} genesis block spec
 */
function buildSpec (name, networkID, ownerAddress, validators) {
  const engine = {
    'authorityRound': {
      'params': {
        'stepDuration': 5,
        'validators': {
          'multi': {
            '0': {
              'list': validators
            }
          }
        },
        'blockReward': '0x2386F26FC10000',
        'maximumUncleCountTransition': 0,
        'maximumUncleCount': 0
      }
    }
  }
  const genesis = {
    'seal': {
      'authorityRound': {
        'step': '0x0',
        'signature': '0x0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000'
      }
    },
    'difficulty': '0x20000',
    'gasLimit': '0x663BE0'
  }
  return build(engine, genesis, name, networkID, ownerAddress, validators)
}

/**
 * Build a genesis block spec
 * @param {string} engine specifications
 * @param {string} genesis block specifications
 * @param {string} name
 * @param {string} networkID
 * @param {string} ownerAddress (of identity contract)
 * @param {stringArray} validators
 * @returns {string} genesis block spec
 */
function build (engine, genesis, name, networkID, ownerAddress, validators) {
  return JSON.stringify({
    'name': name,
    'engine': engine,
    'genesis': genesis,
    'params': {
      'networkID': networkID,
      'maximumExtraDataSize': '0x20',
      'minGasLimit': '0x1388',
      'gasLimitBoundDivisor': '0x400',
      'eip140Transition': '0x0',
      'eip211Transition': '0x0',
      'eip214Transition': '0x0',
      'eip658Transition': '0x0',
      'eip145Transition': '0x0',
      'eip1014Transition': '0x0',
      'eip1052Transition': '0x0'
    },
    'accounts': {
      '0x0000000000000000000000000000000000000001': { 'balance': '1', 'builtin': { 'name': 'ecrecover', 'pricing': { 'linear': { 'base': 3000, 'word': 0 } } } },
      '0x0000000000000000000000000000000000000002': { 'balance': '1', 'builtin': { 'name': 'sha256', 'pricing': { 'linear': { 'base': 60, 'word': 12 } } } },
      '0x0000000000000000000000000000000000000003': { 'balance': '1', 'builtin': { 'name': 'ripemd160', 'pricing': { 'linear': { 'base': 600, 'word': 120 } } } },
      '0x0000000000000000000000000000000000000004': { 'balance': '1', 'builtin': { 'name': 'identity', 'pricing': { 'linear': { 'base': 15, 'word': 3 } } } },
      '0x0000000000000000000000000000000000000005': { 'builtin': { 'name': 'modexp', 'activate_at': '0x0', 'pricing': { 'modexp': { 'divisor': 20 } } } },
      '0x0000000000000000000000000000000000000006': { 'builtin': { 'name': 'alt_bn128_add', 'activate_at': '0x0', 'pricing': { 'linear': { 'base': 500, 'word': 0 } } } },
      '0x0000000000000000000000000000000000000007': { 'builtin': { 'name': 'alt_bn128_mul', 'activate_at': '0x0', 'pricing': { 'linear': { 'base': 40000, 'word': 0 } } } },
      '0x0000000000000000000000000000000000000008': { 'builtin': { 'name': 'alt_bn128_pairing', 'activate_at': '0x0', 'pricing': { 'alt_bn128_pairing': { 'base': 100000, 'pair': 80000, 'eip1108_transition_base': 45000, 'eip1108_transition_pair': 34000 } } } },
      [systemAddresses.identityContract]: { 'balance': '1', 'constructor': identityConstructor(ownerAddress) },
      [ownerAddress]: { 'balance': '1000000000000000000000000000' }
    }
  }, null, 4)
}

module.exports = { systemAddresses, buildFastSpec, buildSpec }
