#!/bin/bash
# Helper script for starting/stopping parity and running tests
#

# You may define CAELUM_PARITY_HOME as desired
if [ -z "${CAELUM_PARITY_HOME}" ]
then
  export CAELUM_PARITY_HOME="$HOME/.caelum"
fi

CAELUM_BIN=$CAELUM_PARITY_HOME/bin
CAELUM_LOGS=$CAELUM_PARITY_HOME/logs

PARITY_RESET=0
PARITY_SPEC_BUILD_OPTION="buildSpec"

# A nonexistent destination directory is the same as a reset 
if [ ! -d "$CAELUM_PARITY_HOME" ]
then
  PARITY_RESET=1
fi

# Process command line flags
while test $# -gt 0
do
    case "$1" in
        --fast) PARITY_SPEC_BUILD_OPTION="buildFastSpec"
            ;;
        --reset) PARITY_RESET=1
            ;;
    esac
    shift
done

# DIR = real location of this script
SOURCE="${BASH_SOURCE[0]}"
while [ -h "$SOURCE" ]; do # resolve $SOURCE until the file is no longer a symlink
  DIR="$( cd -P "$( dirname "$SOURCE" )" >/dev/null 2>&1 && pwd )"
  SOURCE="$(readlink "$SOURCE")"
  [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE" # if $SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
done
DIR="$( cd -P "$( dirname "$SOURCE" )" >/dev/null 2>&1 && pwd )"

# Whenever the machine reboots we get a new copy of parity
DAILY_PARITY_DOWNLOAD="/tmp/caelum-parity-binary"

function installParity() {
  if [ ! -f "$CAELUM_BIN"/parity ]
  then

    if [ ! -f "$DAILY_PARITY_DOWNLOAD" ]
    then
      # Download parity
      # curl -o "$DAILY_PARITY_DOWNLOAD" https://releases.parity.io/ethereum/stable/x86_64-unknown-linux-gnu/parity
      # Lock to version 2.5.7 since 2.5.8 has issues https://gitscrum.com/caelum-labs-2/mbaex/issues/parity-v258-does-not-work
      curl -o "$DAILY_PARITY_DOWNLOAD" https://releases.parity.io/ethereum/v2.5.7/x86_64-unknown-linux-gnu/parity
    fi

    mkdir -p "$CAELUM_BIN"
    cp "$DAILY_PARITY_DOWNLOAD" "$CAELUM_BIN"/parity
    chmod ogu+x "$CAELUM_BIN"/parity
  fi
}

function configureParity() {
  # Set up config directory
  mkdir -p "$CAELUM_PARITY_HOME"
  # create genesis spec file
  pushd "$DIR"/.. > /dev/null
  node index.js $PARITY_SPEC_BUILD_OPTION Alastria 0x201B 0x8ad0ba67f8088d1f990f878815173f1dafda0a55 0x8ad0ba67f8088d1f990f878815173f1dafda0a55 > "$CAELUM_PARITY_HOME"/spec.json
  popd > /dev/null
  # copy other files needed
  cp "$DIR"/../config/authority.toml "$CAELUM_PARITY_HOME"
  cp "$DIR"/../config/pass "$CAELUM_PARITY_HOME"
  mkdir -p "$CAELUM_PARITY_HOME"/parity/keys/Alastria
  cp "$DIR"/../config/UTC* "$CAELUM_PARITY_HOME"/parity/keys/Alastria
  # create the symlink
  . "$DIR"/createSymlink.sh
}

function startupParity() {
  pushd "$CAELUM_PARITY_HOME" > /dev/null
  mkdir -p "$CAELUM_LOGS"
  "$CAELUM_BIN"/parity --config "$CAELUM_PARITY_HOME"/authority.toml >>"$CAELUM_LOGS"/parity.txt 2>&1 &
  export PARITY_PID=$!
  echo "Started new Parity CLI as process $PARITY_PID"
  popd > /dev/null
}

# Stop any existing parity
. "$DIR"/stopParity.sh

# Reset directory if requested
if (( PARITY_RESET == 1 ))
then
  rm -rf "$CAELUM_PARITY_HOME"
  installParity
  configureParity
fi

startupParity
