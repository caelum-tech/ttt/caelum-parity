#!/bin/bash
# Helper script for starting/stopping parity and running tests
#

function shutdownParity() {
  # Support reentrancy
  if [ "$PARITY_PID" == "" ]
  then
    # Find our parity client process
    PARITY_SEARCH="$(ps -ef | grep "parity --config" | grep -v grep | awk '{print $2}')"
    export PARITY_PID="${PARITY_SEARCH}"
  fi
  if [ "$PARITY_PID" != "" ]
  then
    echo "Killing existing Parity CLI process $PARITY_PID"
    kill -9 $PARITY_PID
    # Clean up killed process
    export PARITY_PID=""
  fi
}

shutdownParity
