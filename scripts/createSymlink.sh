#!/bin/bash
# Helper script for creating Parity symlink needed for keythereum
#

function configureSymlink() {
  # Environment variable manadatory
  if [ -z "${CAELUM_PARITY_HOME}" ]
  then
    echo "Environment variable CAELUM_PARITY_HOME must be set."
    exit 1
  fi

  # keythereum appends a keystore subdirectory to the key path,
  # because go-ethereum works that way. So we create a symlink
  # that mirrors that configuration.
  if [ ! -h "$CAELUM_PARITY_HOME"/keystore ]
  then
    ln -s "$CAELUM_PARITY_HOME"/parity/keys/Alastria "$CAELUM_PARITY_HOME"/keystore
    if [ $? -eq 0 ]; then
      echo "created symlink"
    else
      echo "failed to create symlink"
      exit 1
    fi
  fi
}

configureSymlink
