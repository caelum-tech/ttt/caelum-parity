const expect = require('chai').expect
const expectThrow = require('./helpers/expectThrow.js')
const fs = require('fs')
const Parity = require('../index')

describe('Parity', function () {
  let parityJRPC
  let parityIPC
  let wallet

  describe('#constructor()', function () {
    let parityIPCdefault

    it('Should connect using IPC by default', async function () {
      parityIPCdefault = new Parity()
    })

    it('Should connect using IPC', async function () {
      const path = process.env.CAELUM_PARITY_HOME ? process.env.CAELUM_PARITY_HOME : `${process.env.HOME}/.caelum`
      parityIPC = new Parity(path + '/parity/jsonrpc.ipc')
    })

    it('Should connect using JsonRpc', async function () {
      parityJRPC = new Parity('http://localhost:8545')
    })

    it('Should not connect using unsupported Websockets', async function () {
      let testPassed = false
      try {
        new Parity('ws://localhost:8546') // eslint-disable-line no-new
      } catch (e) {
        testPassed = true
      }
      expect(testPassed).to.be.true
    })

    it('Should have connected', async function () {
      await parityIPC.isBlockchainRunning()
      await parityIPCdefault.isBlockchainRunning()
      await parityJRPC.isBlockchainRunning()
    })
  })

  describe('Home directory', function () {
    it('Should return the true path of the home directory', async function () {
      let path = parityIPC.getHomeDirectory()
      if (process.env.CAELUM_PARITY_HOME) { expect(path).to.be.eq(process.env.CAELUM_PARITY_HOME) } else { expect(path).to.be.eq(`${process.env.HOME}/.caelum`) }
    })
  })

  describe('Wallets', function () {
    let wallet
    it('Should create a new wallet', async function () {
      wallet = await parityIPC.addWallet('testpassword')
      expect(wallet.address).to.not.be.empty
    })

    it('Should be able to read from the wallet created', async function () {
      let wallet2 = await parityJRPC.loadWallet(wallet.address, 'testpassword')
      expect(wallet2.address.toUpperCase()).to.be.equal('0X' + wallet.address.toUpperCase())
    })

    it('Should not be able to read from the wallet created with the wrong password', async function () {
      await expectThrow(parityIPC.loadWallet(wallet.address, 'tastepasswd'))
      await expectThrow(parityJRPC.loadWallet(wallet.address, 'tastepasswd'))
    })

    it('Should get the wallet balance', async function () {
      var balance = await parityJRPC.balance(wallet.address)
      expect(Number(balance)).to.be.equal(0)
      balance = await parityIPC.balance(wallet.address)
      expect(Number(balance)).to.be.equal(0)
    })
  })

  describe('System wallet', function () {
    let syswalletfile = fs.readFileSync('./config/UTC--2019-04-18T11-19-16.116Z--8ad0ba67f8088d1f990f878815173f1dafda0a55')
    let syswallet = JSON.parse(syswalletfile)
    let password = fs.readFileSync('./config/pass')
    it('Should be able to read from the system wallet', async function () {
      wallet = await parityIPC.loadWallet(syswallet.address, password)
      expect('0X' + syswallet.address.toUpperCase()).to.be.equal(wallet.address.toUpperCase())
    })
  })

  describe('Deploy and get contract', function () {
    const ensRegistry = require('../node_modules/@caelum-tech/ens/build/contracts/ENSRegistry.json')
    let tx
    it('Should deploy a contract', async function () {
      tx = await parityIPC.deployContract(wallet, ensRegistry)
    })

    it('Should get a contract', async function () {
      await parityIPC.getContract(tx.address, ensRegistry.abi)
    })
  })

  describe('Send transactions', function () {
    it('Should send a transaction', async function () {
      await parityIPC.sendTransaction(wallet, '0x8ad0ba67f8088d1f990f878815173f1dafda0a55', '100000')
    })
  })

  describe('System addresses', function () {
    it('Should be accessible', async function () {
      expect(Parity.getSystemAddresses().identityContract).to.be.not.undefined
    })
  })
})
