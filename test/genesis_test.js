const expect = require('chai').expect
const { exec } = require('child_process')
const genesis = require('../src/genesis')
const owner = '0x8ad0ba67f8088d1f990f878815173f1dafda0a55'

/**
 * Execute simple shell command (async wrapper).
 * @param {String} cmd
 * @return {Object} { stdout: String, stderr: String }
 */
async function sh (cmd) {
  return new Promise(function (resolve, reject) {
    exec(cmd, (err, stdout, stderr) => { // eslint-disable-line handle-callback-err
      resolve({ stdout, stderr })
    })
  })
}

describe('Genesis', function () {
  describe('buildSpec()', function () {
    let result

    it('Should produce a spec', function () {
      result = JSON.parse(genesis.buildSpec('TestNetwork', '0x404', owner, [owner]))
    })

    it('Should handle name', function () {
      expect(result.name).to.equal('TestNetwork')
    })

    it('Should handle network ID', function () {
      expect(result.params.networkID).to.equal('0x404')
    })

    it('Should handle a single validator', async function () {
      expect(result.engine.authorityRound.params.validators.multi['0'].list.length).to.equal(1)
      expect(result.engine.authorityRound.params.validators.multi['0'].list[0]).to.equal(owner)
    })

    it('Should use AuthorityRound', async function () {
      expect(result.genesis.seal.authorityRound.step).to.equal('0x0')
    })

    it('Should set the owner balance with the gas', function () {
      expect(result.accounts[owner].balance).to.equal('1000000000000000000000000000')
    })

    it('Should append the abi encoded owner address to the constructors', function () {
      expect(result.accounts[genesis.systemAddresses.identityContract].constructor).to.contain(owner.substr(2))
    })
  })

  describe('buildFastSpec()', function () {
    let result

    it('Should produce a spec', function () {
      result = JSON.parse(genesis.buildFastSpec('TestNetwork', '0x404', owner, [owner]))
    })

    it('Should be instant seal', async function () {
      expect(result.genesis.seal.generic).to.equal('0x0')
    })
  })

  describe('CLI', function () {
    it('Should provide help when insufficient number of arguments supplied', async function () {
      let { stderr } = await sh('node index buildSpec')
      expect(stderr).to.contain('usage: node index buildSpec')
    })

    it('Should produce valid spec for one validator', async function () {
      let { stdout } = await sh('node index buildSpec NetworkID 0x01 0x8ad0ba67f8088d1f990f878815173f1dafda0a55 0xdb3288f495605dacbff552b3e6c48b9f0e82db5c')
      let result = JSON.parse(stdout)
      expect(result.engine.authorityRound.params.validators.multi['0'].list.length).to.equal(1)
      expect(result.engine.authorityRound.params.validators.multi['0'].list[0]).to.equal('0xdb3288f495605dacbff552b3e6c48b9f0e82db5c')
    })

    it('Should produce valid spec for two validators', async function () {
      let { stdout } = await sh('node index buildSpec NetworkID 0x01 0x8ad0ba67f8088d1f990f878815173f1dafda0a55 0xdb3288f495605dacbff552b3e6c48b9f0e82db5c 0x832b1f863669ef3c96649bcf1d1d04ded595c0ef')
      let result = JSON.parse(stdout)
      expect(result.engine.authorityRound.params.validators.multi['0'].list.length).to.equal(2)
      expect(result.engine.authorityRound.params.validators.multi['0'].list[0]).to.equal('0xdb3288f495605dacbff552b3e6c48b9f0e82db5c')
      expect(result.engine.authorityRound.params.validators.multi['0'].list[1]).to.equal('0x832b1f863669ef3c96649bcf1d1d04ded595c0ef')
    })

    it('Should produce valid spec for three validators', async function () {
      let { stdout } = await sh('node index buildSpec NetworkID 0x01 0x8ad0ba67f8088d1f990f878815173f1dafda0a55 0xdb3288f495605dacbff552b3e6c48b9f0e82db5c 0x832b1f863669ef3c96649bcf1d1d04ded595c0ef 0xdb3288f495605dacbff552b3e6c48b9f0e82db5c')
      let result = JSON.parse(stdout)
      expect(result.engine.authorityRound.params.validators.multi['0'].list.length).to.equal(3)
      expect(result.engine.authorityRound.params.validators.multi['0'].list[0]).to.equal('0xdb3288f495605dacbff552b3e6c48b9f0e82db5c')
      expect(result.engine.authorityRound.params.validators.multi['0'].list[1]).to.equal('0x832b1f863669ef3c96649bcf1d1d04ded595c0ef')
      expect(result.engine.authorityRound.params.validators.multi['0'].list[2]).to.equal('0xdb3288f495605dacbff552b3e6c48b9f0e82db5c')
    })
  })
})
