# caelum-parity
This library contains the configuration and launch scripts for the blockchain.

|Branch|Pipeline|Coverage|
|:-:|:-:|:-:|
|[`master`](https://gitlab.com/caelum-tech/caelum-parity/tree/master)|[![pipeline status](https://gitlab.com/caelum-tech/caelum-parity/badges/master/pipeline.svg)](https://gitlab.com/caelum-tech/caelum-parity/commits/master)|[![coverage report](https://gitlab.com/caelum-tech/caelum-parity/badges/master/coverage.svg)](https://gitlab.com/caelum-tech/caelum-parity/commits/master)|

# Prerequisites
* Ubuntu 18.04
* Node 10.15

## Key setup
* Private key in the directory `$HOME/.caelum/keystore/Alastria`
* Passphase in the file `$HOME/.caelum/keystore/pass`

# Usage

The Caelum blockchain configuration is kept in `$HOME/.caelum` unless otherwise specified in `$CAELUM_PARITY_HOME`.  This is useful in development configurations.

For production use, set `export CAELUM_PARITY_HOME='/data/...'` to the local filesystem directory where the Parity installation is present.  Then invoke `./scripts/createSymlink` to create the necessary symlink for keythereum to find the files it needs.

`$CAELUM_PARITY_KEY_DIRECTORY` can be set to specify the directory in which Parity is storing the keys. This depends on the name of the blockchain, and the path can vary based on installation, so override this with the substitute for `CAELUM_PARITY_HOME/parity/keys/Alastria`.

## Generate a genesis block spec file 

usage:
```
node index build[Fast]Spec networkName 0xNetNum 0xOwnerAddr 0xValidator1 [0xValidator2 ...]
```

`buildSpec` builds a spec file for a regular production-grade network.
`buildFastSpec` builds a spec file for an auto-sealing configuration for testing.

Example: to generate a genesis block with three validator nodes:
```
node index buildSpec NetworkID 0x21 0x8ad0ba67f8088d1f990f878815173f1dafda0a55 0xdb3288f495605dacbff552b3e6c48b9f0e82db5c 0x832b1f863669ef3c96649bcf1d1d04ded595c0ef 0xdb3288f495605dacbff552b3e6c48b9f0e82db5c > spec.json
```


## Development activities
```
npm run start [--fast] [--reset]
```

The `--fast` option starts a network using the InstantSeal engine. Without it, AuthorityRound is used.

The `--reset` option deletes the existing blockchain and starts over.

... do things ...

```
npm run stop
```

# Testing
```
npm i
npm run lint
npm test
```
## Coverage
```
npm run coverage
```
To generate a coverage report in `coverage/index.html`:
```
npm run coverage:report
```
