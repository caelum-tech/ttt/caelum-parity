const ethers = require('ethers')
const genesis = require('./src/genesis.js')
const keythereum = require('keythereum')
const os = require('os')

// Default Tx options.
const defaultDeployOptions = {
  gasLimit: 6000000,
  gasPrice: 1
}
const overrideOptions = {
  gasLimit: 6000000
}

const walletParams = { keyBytes: 32, ivBytes: 16 }

// default working directory name
const workingDir = '.caelum'

/**
 * Parity : handles Interactions with Parity
 */
class Parity {
  /**
   * Constructor.
   * @param {provider} the connection string to use to contact the provider.
   * Default to IPC in the CAELUM_PARITY_HOME or default directory.
   */
  constructor (provider = '') {
    if (provider === '') { provider = this.getHomeDirectory() + '/parity/jsonrpc.ipc' }
    if (provider.substring(0, 4) === 'http') { this.provider = new ethers.providers.JsonRpcProvider(provider) } else if (provider.substring(provider.length - 3, provider.length) === 'ipc') { this.provider = new ethers.providers.IpcProvider(provider) } else { throw (new Error('provider not supported')) }
    // Unfortunately ethers.js does not yet support Websockets https://github.com/ethers-io/ethers.js/issues/141
  }

  /**
   * Returns the pathname of the caelum-parity installation used,
   * by default, $HOME/.caelum or set in $CAELUM_PARITY_HOME
   * @returns {dirname}
   */
  getHomeDirectory () {
    return (process.env.CAELUM_PARITY_HOME ? process.env.CAELUM_PARITY_HOME : `${os.homedir}/${workingDir}`)
  }

  /**
   * Make system addresses available
   */
  static getSystemAddresses () {
    return genesis.systemAddresses
  }

  /**
   * Returns the instance for a contract (abi) in  contractAddr
   * @param {address} contractAddr
   * @param {json} abi
   * @param {ethers.wallet} wallet
   * @returns {ethers.contract}
   */
  getContract (contractAddr, abi, wallet = false) {
    let provider = wallet || this.provider
    return new ethers.Contract(contractAddr, abi, provider)
  }

  /**
   * Deploy new Identity
   * @param {*} address - Wallet (address) owning this Identity.
   */
  async deployId (wallet, contractJSON, address) {
    return new Promise(async (resolve) => {
      // Deploy Identity Contract.
      let factory = new ethers.ContractFactory(contractJSON.abi, contractJSON.bytecode, wallet)
      let contract = await factory.deploy(address)

      // The contract is NOT deployed yet we must wait until it is mined
      await contract.deployed()
      resolve(contract)
    })
  }

  /**
   * Deploys a Smart Contract
   * @paramstring{*} waitingText
   * @param {wallet} wallet
   * @param {json} contractJSON
   * @param {*} args
   * @param {*} overrideOptions
   */
  async deployContract (wallet, contractJSON, args = [], overrideOptions = {}) {
    const bytecode = `${contractJSON.bytecode}`
    const abi = contractJSON.interface ? contractJSON.interface : contractJSON.abi
    const deployTransaction = {
      ...defaultDeployOptions,
      ...overrideOptions,
      ...new ethers.ContractFactory(abi, bytecode).getDeployTransaction(...args)
    }
    const tx = await wallet.sendTransaction(deployTransaction)
    let receipt = await tx.wait()
    return new ethers.Contract(receipt.contractAddress, abi, wallet)
  }

  /**
   * Sends a transaction to the Blockchain
   * @param {wallet} wallet
   * @param {address} addressTo
   * @param {number} total
   */
  async sendTransaction (wallet, addressTo, total) {
    let transaction = {
      nonce: await this.provider.getTransactionCount(wallet.address),
      to: addressTo,
      value: ethers.utils.parseEther(total)
    }
    try {
      let tx = await wallet.sendTransaction(transaction, overrideOptions)
      await tx.wait()
    } catch (e) {
      console.error('Failed transaction' + e)
    }
  }

  /**
   * Send signed Transaction
   * @param {hex} signedTransaction
   */
  async sendSignedTransaction (signedTransaction) {
    return this.provider.sendTransaction(signedTransaction)
  }

  /**
   * Returns a Wallet from a Public Key
   * @param {string} publicKey
   * @param {string} password
   * @returns {ethers.wallet}
   */
  loadWallet (publicKey, password) {
    return new Promise(async (resolve, reject) => {
      try {
        // NOTE: keythereum appends '/keystore/' to the path provided, whether you like it or not.
        // This is inconvenient for Parity, which does not use that subdirectory. There is a
        // symlink created in `./scripts/startParity.sh` which makes this work.
        let keyObject = await keythereum.importFromFile(publicKey, this.getHomeDirectory())
        let wallet = await ethers.Wallet.fromEncryptedJson(JSON.stringify(keyObject), password)
        wallet = wallet.connect(this.provider)
        resolve(wallet)
      } catch (e) {
        reject(e)
      }
    })
  }

  /**
   * Adds a new wallet
   * @param {string} password
   * @returns {keyObject}
   */
  async addWallet (password) {
    return new Promise(async (resolve) => {
      let dk = keythereum.create(walletParams)

      let keyObject = keythereum.dump(password, dk.privateKey, dk.salt, dk.iv)
      keythereum.exportToFile(keyObject,
        process.env.CAELUM_PARITY_KEY_DIRECTORY
          ? process.env.CAELUM_PARITY_KEY_DIRECTORY
          : this.getHomeDirectory() + '/parity/keys/Alastria')
      resolve(keyObject)
    })
  }

  /**
   * Returns the balance for the wallet
   * @param {address} address
   * @returns {number}
   */
  balance (address) {
    return new Promise(async (resolve) => {
      let balance = await this.provider.getBalance(address)
      try {
        resolve(ethers.utils.formatEther(balance))
      } catch (e) {
        // Wallet is fully loaded
        resolve(1000000)
      }
    })
  }

  /**
   * Check if the Blockchain is up and running.
   * @param {boolean} initNetwork - Initlizlizing the network
   * @returns {boolean} - The Blockchain is running
   */
  isBlockchainRunning (initNetwork) {
    return new Promise(async (resolve, reject) => {
      try {
        await this.provider.getBlockNumber()
        resolve(true)
      } catch (e) {
        reject(e)
      }
    })
  }
}

// Exports Parity
module.exports = Parity

// Command-line interface for building genesis blocks
if ((process.argv[2] === 'buildFastSpec') || (process.argv[2] === 'buildSpec')) {
  let validators
  if (process.argv.length < 7) {
    process.stderr.write(`usage: node index ${process.argv[2]} networkName 0xNetNum 0xOwnerAddr 0xValidator1 [0xValidator2 ...]\n`)
    process.exit(1)
  } else if (process.argv.length === 7) {
    validators = [process.argv[6]]
  } else {
    validators = process.argv.slice(6)
  }
  let buildMethod = (process.argv[2] === 'buildFastSpec') ? genesis.buildFastSpec : genesis.buildSpec
  process.stdout.write(buildMethod(process.argv[3], process.argv[4], process.argv[5], validators))
}
